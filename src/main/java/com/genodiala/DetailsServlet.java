package com.genodiala;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6894566209434439164L;

	public void init() throws ServletException{
		System.out.println("****************************");
		System.out.println("DetailsServlet has been initialized.... ");
		System.out.println("****************************");
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		PrintWriter out = res.getWriter(); 
		/*
		 * Branding- ServletConfig Parameter 
		 * ServletConfig servConfig = getServletConfig(); 
		 * String branding = servConfig.getInitParameter("branding");
		 */
		 
		// Branding - Servlet Context Parameter
		ServletContext srvContext = getServletContext();
		 String branding = srvContext.getInitParameter("branding");
		 
		 // First Name - System Properties
		 Properties p = System.getProperties();
		 String firstName = p.getProperty("firstName");
		 
		 //Last Name- HttpSession 
		 HttpSession session = req.getSession();
		 String lastName = session.getAttribute("lastName").toString();

		 //Email - 

		 String email = srvContext.getAttribute("email").toString();
		 
		//Contact Number- Url Rewriting via sendRequest
		 String contactNumber = req.getParameter("contactNumber");	
		 
		 // Name condition
		 String[] firstWords = firstName.split(" ");
		 String capitalizedFirstName = "";
		 for (String word : firstWords) {
		     if (word.length() > 0) {
		         String firstLetter = word.substring(0, 1);
		         String capitalizedFirstLetter = firstLetter.toUpperCase();
		         String restOfWord = word.substring(1);
		         String capitalizedWord = capitalizedFirstLetter + restOfWord;
		         capitalizedFirstName += capitalizedWord + " ";
		     }
		 }
		 String fname = capitalizedFirstName.trim();

		 String firstLetterL = lastName.substring(0, 1);
		 String capitalizedLastName = lastName;

		 if (firstLetterL.matches("[a-z]")) {
			    capitalizedLastName = firstLetterL.toUpperCase() + lastName.substring(1);
		}
		 String lname = capitalizedLastName;
		 
		 //Email and Contact Number Condition
		if (email.matches("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}") && contactNumber.matches("(\\+639|09)\\d{9}")) {
			    out.println("<div align='center'>"
			        + "<h1>" + branding + "</h1>"
			        + "<p> First Name: " + fname + "</p>"
			        + "<p> Last Name: " + lname + "</p>"
			        + "<p> Contact: " + contactNumber + "</p>"
			        + "<p> Email: " + email + "</p>"
			        + "</div>");
		} 
		else {
			 out.println("<div align='center'>"
			        + "<h1> Error. Please enter the correct email or contact number format. </h1>"
			        + "</div>");
		}
	  }
	
	public void destroy() {
		System.out.println("****************************");
		System.out.println("DetailsServlet has been destroyed.... ");
		System.out.println("****************************");
	}
}
