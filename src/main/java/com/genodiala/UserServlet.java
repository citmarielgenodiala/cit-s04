package com.genodiala;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8814759100259548735L;
	
	
	public void init() throws ServletException{
		System.out.println("****************************");
		System.out.println("UserServlet has been initialized... ");
		System.out.println("****************************");
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
	    // First Name- System Properties
		Properties p = System.getProperties();
	    String firstName = req.getParameter("firstName");
	    p.setProperty("firstName", firstName);
	    
	    
	    //Last Name- HttpSession
	    HttpSession session = req.getSession();
	    String lastName = req.getParameter("lastName");
	    session.setAttribute("lastName", lastName);
	    
	    //Email - Servlet Context setAttribute method
	    String email = req.getParameter("email");
	    ServletContext srvContext = getServletContext();
	    srvContext.setAttribute("email", email);
	    
	    //Contact Number - URLRewriting via sendRequest method
	    String contactNumber = req.getParameter("contactNumber");
	    res.sendRedirect("details?contactNumber="+contactNumber);
	    
	  }
	
	public void destroy() {
		System.out.println("****************************");
		System.out.println("UserServlet has been destroyed... ");
		System.out.println("****************************");
	}
}
